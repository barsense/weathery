var express = require('express');
var request = require('request');
var querystring = require('querystring');
var fs = require('fs');

GLOBAL.towns = initTowns();

var app = express();

app.use(express.static(__dirname + '/public'));


app.get("/api/weather/:type", function (req, res) {
	var type = req.params['type'];
	var size = req.query['size'];
	var top = parseFloat(req.query['top']);
	var right = parseFloat(req.query['right']);
	var bottom = parseFloat(req.query['bottom']);
	var left = parseFloat(req.query['left']);

	woeids = "";
	for (var i=0; i<towns.length; i++) {
		//find for all towns in range
		if (towns[i]['lat'] <= top && towns[i]['lat'] >= bottom &&
			towns[i]['lng'] <= right && towns[i]['lng'] >= left) {
			woeids +=towns[i]['woeid']+", ";
		}
	}
	woeids = woeids.slice(0, woeids.length-2)
	url = "https://query.yahooapis.com/v1/public/yql?q="+
		querystring.escape("select * from weather.forecast where woeid in ("+woeids+")")+"&format=json"

	request(url, function(error, response, body) {
		rawResult = JSON.parse(body);
		if (rawResult['error']) {
			res.send([]);
		}
		else {
			results = JSON.parse(body)['query']['results']['channel'];
			weather = [];

			for (var i=0; i<results.length; i++) {
				weather.push({
					"name": results[i]['location']['city'],
					"lat":results[i]['item']['lat'],
					"lng":results[i]['item']['long'],
					"temp":Math.round((parseFloat(results[i]['item']['condition']['temp'])-32)/1.8)
				});
			}
			
			weather.sort(function (lhs, rhs) {return rhs['temp']-lhs['temp']});
			if (type=="hot") {
				res.send(weather.slice(0, size));
			}
			else {
				res.send(weather.slice(-size));
			}
		}
	});
});


function initTowns() {
	data = JSON.parse(fs.readFileSync(__dirname+'/counties.json', 'utf8'))
   	rawTowns = data['query']['results']['place'];
   	towns = [];
	for (var i=0; i< rawTowns.length; i++) {
		towns.push({
			"name":rawTowns[i]['name'],
			"woeid":rawTowns[i]['woeid'],
			"lat":parseFloat(rawTowns[i]['centroid']['latitude']),
			"lng":parseFloat(rawTowns[i]['centroid']['longitude'])
		});
	}
	return towns;
}

var server = app.listen(8080);