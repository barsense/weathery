
var map;
var markers = [];

function initMap() {
    var mapOptions = {
        zoom: 4,
        center: new google.maps.LatLng(60, -100),
        disableDefaultUI: true,
        zoomControl:true
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
}

function updateMap() {
    $("#loading").show();
    var size = $("#size")[0].value;
    var type = $("#type")[0].value;
    var bounds = map.getBounds();
    var params = {
        "size": size,
        "top": bounds.getNorthEast().lat(),
        "right": bounds.getNorthEast().lng(),
        "bottom": bounds.getSouthWest().lat(),
        "left": bounds.getSouthWest().lng()
    }
    
    $.ajax('/api/weather/'+type+"?" + $.param(params), {
        success: function (data, status) {
            $.each(markers, function (index, marker) {
                marker.setMap(null);
            })
            markers = [];
            $.each(data, function(index, town) {
                var marker = new MarkerWithLabel({
                    icon: '/img/town.png',
                    position: new google.maps.LatLng(town['lat'], town['lng']),
                    map: map,
                    labelContent: town['name']+": "+town['temp']+"℃",
                    labelClass: 'labels'
                });
                markers.push(marker);
            });
        },
        error: function() {alert("error");},
        complete: function() {$("#loading").hide();}
    });
    
}

$(document).ready(function () {
    initMap();
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(pos);
            map.setZoom(9);
        });
    }

    $("#size, #type").change(updateMap);
    google.maps.event.addListener(map, 'idle', updateMap);
});
